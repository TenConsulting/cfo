# -*- coding: utf-8 -*-
"""
azure KeyPair
"""
from os import getcwd, chmod, remove
from os.path import join
from logging import getLogger
from cryptography.hazmat.primitives import serialization as crypto_serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend as crypto_default_backend
from ..constants import DIR_SSH_KEYS
LOGGER = getLogger(__name__)


def create(kp_name: str):
    'Create Key Pair'
    key = rsa.generate_private_key(
        backend=crypto_default_backend(), public_exponent=65537, key_size=2048)
    # Private Key
    private_key = key.private_bytes(crypto_serialization.Encoding.PEM,
                                    crypto_serialization.PrivateFormat.PKCS8,
                                    crypto_serialization.NoEncryption())
    f_private_key = join(DIR_SSH_KEYS, '{}.pem'.format(kp_name))
    log = '{msg:.<8} Saving private key: {}'.format(
        f_private_key, msg='KEYPAIR')
    LOGGER.info(log.replace('{}'.format(getcwd()), ''))
    with open(f_private_key, 'wb') as f_private_key_content:
        f_private_key_content.write(private_key)
    chmod(f_private_key, 0o400)
    # Public Key
    public_key = key.public_key().public_bytes(
        crypto_serialization.Encoding.OpenSSH,
        crypto_serialization.PublicFormat.OpenSSH)
    f_public_key = join(DIR_SSH_KEYS, '{}.pub'.format(kp_name))
    log = '{msg:.<8} Saving public key: {}'.format(f_public_key, msg='KEYPAIR')
    LOGGER.info(log.replace('{}'.format(getcwd()), ''))
    with open(f_public_key, 'wb') as f_public_key_content:
        f_public_key_content.write(public_key)
    chmod(f_public_key, 0o400)
    return public_key


def delete(kp_name: str):
    'Delete Key Pair'
    # Deleting KeyPair
    log = '{msg:.<8} Deleting KeyPair: {}'.format(kp_name, msg='KEYPAIR')
    LOGGER.info(log)
    # Public Key
    f_public_key = join(DIR_SSH_KEYS, '{}.pub'.format(kp_name))
    log = '{msg:.<8} Removing public key: {}'.format(
        f_public_key, msg='KEYPAIR')
    LOGGER.info(log.replace('{}'.format(getcwd()), ''))
    remove(f_public_key)
    # Private Key
    f_private_key = join(DIR_SSH_KEYS, '{}.pem'.format(kp_name))
    log = '{msg:.<8} Removing private key: {}'.format(
        f_private_key, msg='KEYPAIR')
    LOGGER.info(log.replace('{}'.format(getcwd()), ''))
    remove(f_private_key)
