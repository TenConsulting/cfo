# -*- coding: utf-8 -*-
"""
azure network
"""
from logging import getLogger
from .services import NETWORK, AZURE_LOCATION
LOGGER = getLogger(__name__)


def create_nsg(rg_name, nsg_name):
    'Create Network Security Group'
    log = '{msg:.<8} Creating Network Security Group: {}'.format(
        nsg_name, msg='NETWORK')
    LOGGER.info(log)
    response = NETWORK.network_security_groups.create_or_update(
        rg_name, nsg_name, {
            'location':
            AZURE_LOCATION,
            'security_rules': [{
                'name': 'AllowHTTPInBound',
                'priority': 200,
                'source_address_prefix': '*',
                'protocol': 'Tcp',
                'source_port_range': '*',
                'destination_address_prefix': '*',
                'destination_port_range': '80',
                'access': 'Allow',
                'direction': 'Inbound'
            }, {
                'name': 'AllowHTTPSInBound',
                'priority': 201,
                'source_address_prefix': '*',
                'protocol': 'Tcp',
                'source_port_range': '*',
                'destination_address_prefix': '*',
                'destination_port_range': '443',
                'access': 'Allow',
                'direction': 'Inbound'
            }, {
                'name': 'AllowSSHInBound',
                'priority': 202,
                'source_address_prefix': '*',
                'protocol': 'Tcp',
                'source_port_range': '*',
                'destination_address_prefix': '*',
                'destination_port_range': '22',
                'access': 'Allow',
                'direction': 'Inbound'
            }, {
                'name': 'AllowRDPInBound',
                'priority': 203,
                'source_address_prefix': '*',
                'protocol': 'Tcp',
                'source_port_range': '*',
                'destination_address_prefix': '*',
                'destination_port_range': '3389',
                'access': 'Allow',
                'direction': 'Inbound'
            }, {
                'name': 'AllowVNCInBound',
                'priority': 204,
                'source_address_prefix': '*',
                'protocol': 'Tcp',
                'source_port_range': '*',
                'destination_address_prefix': '*',
                'destination_port_range': '5901',
                'access': 'Allow',
                'direction': 'Inbound'
            }]
        })
    response.wait()
    log = '{msg:.<8} OK'.format(msg='NETWORK')
    LOGGER.info(log)


def delete_nsg(rg_name, nsg_name):
    'Delete Network Security Group'
    log = '{msg:.<8} Deleting Network Security Group: {}'.format(
        nsg_name, msg='NETWORK')
    LOGGER.info(log)
    response = NETWORK.network_security_groups.delete(rg_name, nsg_name)
    response.wait()
    log = '{msg:.<8} OK'.format(msg='NETWORK')
    LOGGER.info(log)


def create_vnet(rg_name, vnet_name):
    'Create vnet'
    log = '{msg:.<8} Creating vnet: {}'.format(vnet_name, msg='NETWORK')
    LOGGER.info(log)
    response = NETWORK.virtual_networks.create_or_update(
        rg_name, vnet_name, {
            'location': AZURE_LOCATION,
            'address_space': {
                'address_prefixes': ['10.0.0.0/16']
            }
        })
    response.wait()
    log = '{msg:.<8} OK'.format(msg='NETWORK')
    LOGGER.info(log)


def delete_vnet(rg_name, vnet_name):
    'Delete vnet'
    log = '{msg:.<8} Deleting vnet: {}'.format(vnet_name, msg='NETWORK')
    LOGGER.info(log)
    response = NETWORK.virtual_networks.delete(rg_name, vnet_name)
    response.wait()
    log = '{msg:.<8} OK'.format(msg='NETWORK')
    LOGGER.info(log)


def create_subnet(rg_name, vnet_name, subnet_name, nsg_name):
    'Create vnet'
    log = '{msg:.<8} Creating subnet: {}'.format(subnet_name, msg='NETWORK')
    LOGGER.info(log)
    nsg = NETWORK.network_security_groups.get(rg_name, nsg_name)
    response = NETWORK.subnets.create_or_update(
        rg_name, vnet_name, subnet_name, {
            'address_prefix': '10.0.0.0/28',
            'network_security_group': nsg
        })
    response.wait()
    log = '{msg:.<8} OK'.format(msg='NETWORK')
    LOGGER.info(log)

def delete_subnet(rg_name, vnet_name, subnet_name):
    'Delete vnet'
    log = '{msg:.<8} Deleting subnet: {}'.format(subnet_name, msg='NETWORK')
    LOGGER.info(log)
    response = NETWORK.subnets.delete(rg_name, vnet_name, subnet_name)
    response.wait()
    log = '{msg:.<8} OK'.format(msg='NETWORK')
    LOGGER.info(log)


def create_pubip(rg_name, pubip_name):
    'Create a Public IP Address'
    log = '{msg:.<8} Creating Public IP: {}'.format(pubip_name, msg='NETWORK')
    LOGGER.info(log)
    response = NETWORK.public_ip_addresses.create_or_update(
        rg_name, pubip_name, {
            'location': AZURE_LOCATION,
            'public_ip_address_version': 'IPv4',
            'public_ip_allocation_method': 'Dynamic',
            'idle_timeout_in_minutes': 4
        })
    response.wait()
    log = '{msg:.<8} OK'.format(msg='NETWORK')
    LOGGER.info(log)


def delete_pubip(rg_name, pubip_name):
    'Delete Public IP Address'
    log = '{msg:.<8} Deleting Public IP Address: {}'.format(
        pubip_name, msg='NETWORK')
    LOGGER.info(log)
    response = NETWORK.public_ip_addresses.delete(rg_name, pubip_name)
    response.wait()
    log = '{msg:.<8} OK'.format(msg='NETWORK')
    LOGGER.info(log)


def get_pubip(rg_name, pubip_name):
    'Delete Public IP Address'
    pubip = NETWORK.public_ip_addresses.get(rg_name, pubip_name)
    return pubip.ip_address


def create_ni(rg_name, ni_name, ipc_name, pubip_name, nsg_name, vnet_name,
              subnet_name):
    'Create Network Interface'
    log = '{msg:.<8} Creating Network Interface: {}'.format(
        ni_name, msg='NETWORK')
    LOGGER.info(log)
    nsg = NETWORK.network_security_groups.get(rg_name, nsg_name)
    subnet = NETWORK.subnets.get(rg_name, vnet_name, subnet_name)
    pubip = NETWORK.public_ip_addresses.get(rg_name, pubip_name)
    response = NETWORK.network_interfaces.create_or_update(
        rg_name, ni_name, {
            'location':
            AZURE_LOCATION,
            'network_security_group':
            nsg,
            'ip_configurations': [{
                'name': ipc_name,
                'subnet': subnet,
                'public_ip_address': pubip
            }]
        })
    response.wait()
    log = '{msg:.<8} OK'.format(msg='NETWORK')
    LOGGER.info(log)


def delete_ni(rg_name, ni_name):
    'Delete Network Interface'
    log = '{msg:.<8} Deleting Network Interface: {}'.format(
        ni_name, msg='NETWORK')
    LOGGER.info(log)
    response = NETWORK.network_interfaces.delete(rg_name, ni_name)
    response.wait()
    log = '{msg:.<8} OK'.format(msg='NETWORK')
    LOGGER.info(log)
