# -*- coding: utf-8 -*-
"""
azure resource groups
"""
from logging import getLogger
from .services import RES_MGR, AZURE_LOCATION
LOGGER = getLogger(__name__)


def list_groups():
    'List all resource groups.'
    print('Resource Groups:')
    for group in RES_MGR.resource_groups.list():
        print(group.name)


def list_resources(rg_name):
    'List all of the resources within the group'
    print('{} Resources:'.format(rg_name))
    for resource in RES_MGR.resources.list_by_resource_group(rg_name):
        print(resource.name)


def create_group(rg_name):
    'Create Resource Group'
    log = '{msg:.<8} Creating Resource Group: {}'.format(rg_name, msg='RESGRP')
    LOGGER.info(log)
    resource_group_params = {'location': AZURE_LOCATION}
    response = RES_MGR.resource_groups.create_or_update(
        rg_name, resource_group_params)
    log = '{msg:.<8} {}'.format(
        response.properties.provisioning_state, msg='RESGRP')
    LOGGER.info(log)


def delete_group(rg_name):
    'Delete Resource Group'
    log = '{msg:.<8} Deleting Resource Group: {}'.format(rg_name, msg='RESGRP')
    LOGGER.info(log)
    response = RES_MGR.resource_groups.delete(rg_name)
    response.wait()
    log = '{msg:.<8} OK'.format(msg='RESGRP')
    LOGGER.info(log)
