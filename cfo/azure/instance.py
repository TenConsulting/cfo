# -*- coding: utf-8 -*-
"""
azure instances
"""
from logging import getLogger
from .services import AZURE_LOCATION, NETWORK
from . import res_grp, network, vm, disks
from .. import generate
LOGGER = getLogger(__name__)


def create(instance_name):
    'Create Instance'
    log = '{msg:.<8} Creating Instance {}'.format(
        instance_name, msg='INSTANCE')
    LOGGER.info(log)
    # Initialize variables
    rg_name = 'rg-{}'.format(instance_name)
    nsg_name = 'nsg-{}'.format(instance_name)
    vnet_name = 'vnet-{}'.format(instance_name)
    subnet_name = 'subnet-{}'.format(instance_name)
    ni_name = 'ni-{}'.format(instance_name)
    pubip_name = 'pubip-{}'.format(instance_name)
    ipc_name = 'ip-{}'.format(instance_name)
    os_disk_name = 'osd-{}'.format(instance_name)
    vm_name = 'vm-{}'.format(instance_name)
    # kp_name = 'cfo_{}_rsa'.format(instance_name)
    # Create
    res_grp.create_group(rg_name)
    network.create_nsg(rg_name, nsg_name)
    network.create_pubip(rg_name, pubip_name)
    network.create_vnet(rg_name, vnet_name)
    network.create_subnet(rg_name, vnet_name, subnet_name, nsg_name)
    network.create_ni(rg_name, ni_name, ipc_name, pubip_name, nsg_name,
                      vnet_name, subnet_name)
    network_interface = NETWORK.network_interfaces.get(rg_name, ni_name)
    # keypair.create(kp_name)
    new_password = generate.password()
    log = '{msg:.<8} Password: {}'.format(new_password, msg='INSTANCE')
    LOGGER.info(log)
    vm_parameters = {
        'location': AZURE_LOCATION,
        'hardware_profile': {
            'vm_size': 'Standard_A1'
        },
        'storage_profile': {
            'image_reference': {
                'publisher': 'Canonical',
                'offer': 'UbuntuServer',
                'sku': '16.04.0-LTS',
                'version': 'latest'
            },
            'os_disk': {
                'create_option': 'FromImage',
                'os_type': 'Linux',
                'name': os_disk_name
            }
        },
        'os_profile': {
            'computer_name': 'workspace',
            'admin_username': 'ubuntu',
            'admin_password': new_password
        },
        'network_profile': {
            'network_interfaces': [{
                'id': network_interface.id
            }]
        }
    }
    vm.create(rg_name, vm_name, vm_parameters)
    pubip_address = network.get_pubip('rg-chess', 'pubip-chess')
    print('ssh ubuntu@{}'.format(pubip_address))
    log = '{msg:.<8} OK'.format(msg='INSTANCE')
    LOGGER.info(log)


def delete(instance_name):
    'Delete Instance'
    log = '{msg:.<8} Deleting Instance {}'.format(
        instance_name, msg='INSTANCE')
    LOGGER.info(log)
    # Initialize variables
    rg_name = 'rg-{}'.format(instance_name)
    nsg_name = 'nsg-{}'.format(instance_name)
    vnet_name = 'vnet-{}'.format(instance_name)
    subnet_name = 'subnet-{}'.format(instance_name)
    pubip_name = 'pubip-{}'.format(instance_name)
    ni_name = 'ni-{}'.format(instance_name)
    os_disk_name = 'osd-{}'.format(instance_name)
    vm_name = 'vm-{}'.format(instance_name)
    # kp_name = 'cfo_{}_rsa'.format(instance_name)
    # Delete
    # keypair.delete(kp_name)
    vm.delete(rg_name, vm_name)
    disks.delete(rg_name, os_disk_name)
    network.delete_ni(rg_name, ni_name)
    network.delete_subnet(rg_name, vnet_name, subnet_name)
    network.delete_vnet(rg_name, vnet_name)
    network.delete_pubip(rg_name, pubip_name)
    network.delete_nsg(rg_name, nsg_name)
    res_grp.delete_group(rg_name)
    log = '{msg:.<8} OK'.format(msg='INSTANCE')
    LOGGER.info(log)
