# -*- coding: utf-8 -*-
"""
azure services
"""
from os import environ
from azure.common.credentials import ServicePrincipalCredentials
from azure.mgmt.resource import ResourceManagementClient
from azure.mgmt.network import NetworkManagementClient
from azure.mgmt.compute import ComputeManagementClient

AZURE_SUBSCRIPTION_ID = environ.get('AZURE_SUBSCRIPTION_ID')
AZURE_CREDENTIALS = ServicePrincipalCredentials(
    tenant=environ.get('AZURE_TENANT_ID'),
    client_id=environ.get('AZURE_CLIENT_ID'),
    secret=environ.get('AZURE_CLIENT_SECRET'))
AZURE_LOCATION = environ.get('AZURE_LOCATION')

RES_MGR = ResourceManagementClient(AZURE_CREDENTIALS, AZURE_SUBSCRIPTION_ID)
COMPUTE = ComputeManagementClient(AZURE_CREDENTIALS, AZURE_SUBSCRIPTION_ID)
NETWORK = NetworkManagementClient(AZURE_CREDENTIALS, AZURE_SUBSCRIPTION_ID)
