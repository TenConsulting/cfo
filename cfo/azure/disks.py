# -*- coding: utf-8 -*-
"""
azure disks
"""
from logging import getLogger
from azure.mgmt.compute.models import DiskCreateOption
from .services import COMPUTE, AZURE_LOCATION
LOGGER = getLogger(__name__)


def create(rg_name, disk_name, size=10):
    'Create Data disk'
    log = '{msg:.<8} Creating Data Disk: {}'.format(disk_name, msg='DISKS')
    LOGGER.info(log)
    response = COMPUTE.disks.create_or_update(
        rg_name, disk_name, {
            'location': AZURE_LOCATION,
            'disk_size_gb': size,
            'creation_data': {
                'create_option': DiskCreateOption.empty
            }
        })
    response.wait()
    log = '{msg:.<8} OK'.format(msg='DISKS')
    LOGGER.info(log)


def delete(rg_name, disk_name):
    'Delete disk'
    log = '{msg:.<8} Deleting Disk: {}'.format(disk_name, msg='DISKS')
    LOGGER.info(log)
    response = COMPUTE.disks.delete(rg_name, disk_name)
    response.wait()
    log = '{msg:.<8} OK'.format(msg='DISKS')
    LOGGER.info(log)
