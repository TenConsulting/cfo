# -*- coding: utf-8 -*-
"""
azure virtual machines
"""
from logging import getLogger
from azure.mgmt.compute.models import DiskCreateOption
from .services import COMPUTE
LOGGER = getLogger(__name__)


def list_all():
    'List VMs in subscription'
    print('List of VMs:')
    for machine in COMPUTE.virtual_machines.list_all():
        print(machine.name)


def list_machines(rg_name):
    'List VMs in resource group'
    print('List of VMs in resource group {}:'.format(rg_name))
    for machine in COMPUTE.virtual_machines.list(rg_name):
        print(machine.name)


def create(rg_name, vm_name, vm_parameters):
    'Create Virtual Machine'
    log = '{msg:.<8} Creating VM: {}'.format(vm_name, msg='VM')
    LOGGER.info(log)
    response = COMPUTE.virtual_machines.create_or_update(
        rg_name, vm_name, vm_parameters)
    response.wait()
    log = '{msg:.<8} OK'.format(msg='VM')
    LOGGER.info(log)


def delete(rg_name, vm_name):
    'Delete Virtual Machine'
    log = '{msg:.<8} Deleting VM: {}'.format(vm_name, msg='VM')
    LOGGER.info(log)
    response = COMPUTE.virtual_machines.delete(rg_name, vm_name)
    response.wait()
    log = '{msg:.<8} OK'.format(msg='VM')
    LOGGER.info(log)


def start(rg_name, vm_name):
    'Start Virtual Machine'
    log = '{msg:.<8} Starting VM: {}'.format(vm_name, msg='VM')
    LOGGER.info(log)
    response = COMPUTE.virtual_machines.start(rg_name, vm_name)
    response.wait()
    log = '{msg:.<8} OK'.format(msg='VM')
    LOGGER.info(log)


def restart(rg_name, vm_name):
    'Restart Virtual Machine'
    log = '{msg:.<8} Restarting VM: {}'.format(vm_name, msg='VM')
    LOGGER.info(log)
    response = COMPUTE.virtual_machines.restart(rg_name, vm_name)
    response.wait()
    log = '{msg:.<8} OK'.format(msg='VM')
    LOGGER.info(log)


def stop(rg_name, vm_name):
    'Stop Virtual Machine'
    log = '{msg:.<8} Stopping VM: {}'.format(vm_name, msg='VM')
    LOGGER.info(log)
    response = COMPUTE.virtual_machines.power_off(rg_name, vm_name)
    response.wait()
    log = '{msg:.<8} OK'.format(msg='VM')
    LOGGER.info(log)


def resize(rg_name, vm_name, size=100):
    'Resize Virtual Machine'
    log = '{msg:.<8} Deleting VM: {}'.format(vm_name, msg='VM')
    LOGGER.info(log)
    response = COMPUTE.virtual_machines.deallocate(rg_name, vm_name)
    response.wait()
    virtual_machine = COMPUTE.virtual_machines.get(rg_name, vm_name)
    os_disk_name = virtual_machine.storage_profile.os_disk.name
    os_disk = COMPUTE.disks.get(rg_name, os_disk_name)
    os_disk.disk_size_gb = size
    response = COMPUTE.disks.create_or_update(rg_name, os_disk.name, os_disk)
    response.wait()
    start(rg_name, vm_name)
    log = '{msg:.<8} OK'.format(msg='VM')
    LOGGER.info(log)


def attach_disk(rg_name, vm_name, disk_name):
    'Attach disk to the Virtual Machine'
    log = '{msg:.<8} Attach Disk {} to VM {}'.format(
        disk_name, vm_name, msg='VM')
    LOGGER.info(log)
    virtual_machine = COMPUTE.virtual_machines.get(rg_name, vm_name)
    data_disk = COMPUTE.disks.get(rg_name, disk_name)
    virtual_machine.storage_profile.data_disks.append({
        'lun':
        len(virtual_machine.storage_profile.data_disks) + 1,
        'name':
        disk_name,
        'create_option':
        DiskCreateOption.attach,
        'managed_disk': {
            'id': data_disk.id
        }
    })
    response = COMPUTE.virtual_machines.create_or_update(
        rg_name, vm_name, virtual_machine)
    response.wait()
    log = '{msg:.<8} OK'.format(msg='VM')
    LOGGER.info(log)


def detach_disk(rg_name, vm_name, disk_name):
    'Detach disk from the Virtual Machine'
    log = '{msg:.<8} Detach Disk {} from VM {}'.format(
        disk_name, vm_name, msg='VM')
    LOGGER.info(log)
    virtual_machine = COMPUTE.virtual_machines.get(rg_name, vm_name)
    data_disks = virtual_machine.storage_profile.data_disks
    data_disks[:] = [disk for disk in data_disks if disk.name != disk_name]
    response = COMPUTE.virtual_machines.create_or_update(
        rg_name, vm_name, virtual_machine)
    response.wait()
    log = '{msg:.<8} OK'.format(msg='VM')
    LOGGER.info(log)
