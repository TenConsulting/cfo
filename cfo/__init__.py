# -*- coding: utf-8 -*-
"""
CFO
"""
from os import makedirs
from os.path import exists
from logging import getLogger, DEBUG, ERROR, Formatter, FileHandler, StreamHandler
from dotenv import load_dotenv
from .constants import F_ENV, DIR_DATA, DIR_LOGS, DIR_SSH_KEYS, LOG_CFO
load_dotenv(F_ENV)


def create_folder(folder_name: str):
    """
    Create a folder with the given name

    Args:
        * folder_name: Folder fullpath
    """
    if not exists(folder_name):
        makedirs(folder_name)


# Create required folders
create_folder(DIR_DATA)
create_folder(DIR_LOGS)
create_folder(DIR_SSH_KEYS)

# Logger
FILE_FORMATTER = Formatter(
    '{levelname:.<8} {asctime} {msecs:010.6f} {name:.<30.30} {message}',
    '%Y-%m-%d %H:%M:%S', '{')
FILE_HANDLER = FileHandler(LOG_CFO)
FILE_HANDLER.setLevel(DEBUG)
FILE_HANDLER.setFormatter(FILE_FORMATTER)
CONSOLE_FORMATTER = Formatter(
    '{levelname:.<8} {asctime} {msecs:010.6f} {name:.<30.30} {message}',
    '%Y-%m-%d %H:%M:%S', '{')
CONSOLE_HANDLER = StreamHandler()
CONSOLE_HANDLER.setLevel(DEBUG)
CONSOLE_HANDLER.setFormatter(CONSOLE_FORMATTER)
LOGGER = getLogger(__name__)
LOGGER.setLevel(DEBUG)
LOGGER.addHandler(FILE_HANDLER)
LOGGER.addHandler(CONSOLE_HANDLER)
