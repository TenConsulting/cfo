# -*- coding: utf-8 -*-
"""
constants
"""
from os import getcwd
from os.path import join, expanduser
from datetime import datetime

# Environment Variables
F_ENV = join(getcwd(), '.env')

# Directories and files
DIR_DATA = join(getcwd(), 'data')
DIR_SSH_KEYS = join(expanduser('~'), '.ssh')
F_SSH_CONFIG = join(DIR_SSH_KEYS, 'config')

# Logger
NOW = datetime.now().strftime('%Y-%m-%d_%H-%M-%S_%f')
TODAY = datetime.now().strftime('%Y-%m-%d')
DIR_LOGS = join(getcwd(), 'logs', TODAY)
LOG_CFO = join(DIR_LOGS, '{}.log'.format(NOW))
