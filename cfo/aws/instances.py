# -*- coding: utf-8 -*-
"""
aws ec2 instances
"""
import re
from logging import getLogger
from .dynamodb import c_item, u_item, g_item, d_item
from .keypair import c_key_pair, d_key_pair
from .sec_grp import c_sec_grp, d_sec_grp
from .services import EC2
from ..constants import F_SSH_CONFIG
LOGGER = getLogger(__name__)


def create_instance(instance_name):
    'Create Instance'
    log = '{msg:.<8} Instance Name: {}'.format(instance_name, msg='STARTED')
    LOGGER.info(log)
    c_item(instance_name)
    f_private_key = c_key_pair(instance_name)
    security_group_id = c_sec_grp(instance_name)
    log = '{msg:.<8} Creating Instance: {}'.format(
        instance_name, msg='INSTANCE')
    LOGGER.info(log)
    instances = EC2.create_instances(
        ImageId='ami-a163c7d9',
        MinCount=1,
        MaxCount=1,
        InstanceType='t2.medium',
        KeyName=instance_name,
        SecurityGroupIds=[security_group_id],
        TagSpecifications=[{
            'ResourceType': 'instance',
            'Tags': [{
                'Key': 'Name',
                'Value': instance_name
            }]
        }])
    # Instance ID
    instance_id = instances[0].instance_id
    log = '{msg:.<8} Instance ID: {}'.format(instance_id, msg='INSTANCE')
    u_item(instance_name, 'InstanceID', 'S', instance_id)
    # Wait until running
    instance = EC2.Instance(instance_id)
    instance.wait_until_running()
    # Public DNS Name
    public_dns_name = instance.public_dns_name
    log = '{msg:.<8} Public DNS Name: {}'.format(
        public_dns_name, msg='INSTANCE')
    LOGGER.info(log)
    u_item(instance_name, 'PublicDNSName', 'S', public_dns_name)
    # Done
    log = '{msg:.<8} Instance Name: {}'.format(instance_name, msg='DONE')
    LOGGER.info(log)
    # Adding ssh config
    config_content = """
# {name} start
Host {name}
  HostName                {}
  LogLevel                ERROR
  User                    ubuntu
  StrictHostKeyChecking   no
  UserKnownHostsFile      /dev/null
  IdentityFile            {key}
# {name} end
    """.format(
        public_dns_name, name=instance_name, key=f_private_key)
    with open(F_SSH_CONFIG, 'a') as ssh_config:
        ssh_config.write(config_content)
    command = 'ssh {}'.format(instance_name)
    print(command)


def delete_instance(instance_name: str):
    'Delete instance'
    log = '{msg:.<8} Instance Name: {}'.format(instance_name, msg='STARTED')
    LOGGER.info(log)
    instance_id = g_item(instance_name)['InstanceID']['S']
    log = '{msg:.<8} Terminating Instance: {}'.format(
        instance_name, msg='INSTANCE')
    LOGGER.info(log)
    instance = EC2.Instance(instance_id)
    responses = instance.terminate()
    instance.wait_until_terminated()
    responses.update()
    for response in responses['TerminatingInstances']:
        log = '{msg:.<8} CurrentState: {}'.format(
            response['CurrentState']['Name'], msg='INSTANCE')
        LOGGER.info(log)
    d_sec_grp(instance_name)
    # Remove SSH config
    with open(F_SSH_CONFIG, 'r') as ssh_config:
        config_content = ssh_config.read()
        pattern_start = '# {} start'.format(instance_name)
        pattern_end = '# {} end'.format(instance_name)
        pattern = r'{}(.*){}'.format(pattern_start, pattern_end)
        new_config_content = re.sub(
            pattern, '', config_content, flags=re.DOTALL)
    with open(F_SSH_CONFIG, 'w') as ssh_config:
        ssh_config.write(new_config_content)
    d_key_pair(instance_name)
    d_item(instance_name)
    log = '{msg:.<8} Instance Name: {}'.format(instance_name, msg='DONE')
    LOGGER.info(log)
