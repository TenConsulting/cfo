# -*- coding: utf-8 -*-
"""
aws DynamoDB
"""
from logging import getLogger
from .services import DYNAMODB
LOGGER = getLogger(__name__)


def c_item(instance_name: str):
    'Create cfo-instance Item'
    log = '{msg:.<8} Creating Item: {}'.format(instance_name, msg='DYNAMODB')
    LOGGER.info(log)
    response = DYNAMODB.put_item(
        TableName='cfo-instance', Item={
            'InstanceName': {
                'S': instance_name
            }
        })
    log = '{msg:.<8} {}'.format(
        response['ResponseMetadata']['HTTPStatusCode'], msg='DYNAMODB')


def u_item(instance_name: str, col_data: str, type_data: str, val_data: str):
    'Update cfo-instance Item'
    if col_data in ['PrivateKey', 'PublicKey']:
        log = '{msg:.<8} Updating Item: {} => {}'.format(
            instance_name, col_data, msg='DYNAMODB')
    else:
        log = '{msg:.<8} Updating Item: {} => {} = {}'.format(
            instance_name, col_data, val_data, msg='DYNAMODB')
    LOGGER.info(log)
    response = DYNAMODB.update_item(
        TableName='cfo-instance',
        Key={'InstanceName': {
            'S': instance_name
        }},
        UpdateExpression='SET {} = :data'.format(col_data),
        ExpressionAttributeValues={
            ':data': {
                type_data: val_data
            }
        })
    log = '{msg:.<8} {}'.format(
        response['ResponseMetadata']['HTTPStatusCode'], msg='DYNAMODB')
    LOGGER.info(log)


def d_item(instance_name: str):
    'Delete cfo-instance Item'
    log = '{msg:.<8} Deleting Item: {}'.format(instance_name, msg='DYNAMODB')
    LOGGER.info(log)
    response = DYNAMODB.delete_item(
        TableName='cfo-instance', Key={
            'InstanceName': {
                'S': instance_name
            }
        })
    log = '{msg:.<8} {}'.format(
        response['ResponseMetadata']['HTTPStatusCode'], msg='DYNAMODB')
    LOGGER.info(log)


def g_item(instance_name: str):
    'Get cfo-instance Item'
    log = '{msg:.<8} Get Item: {}'.format(instance_name, msg='DYNAMODB')
    LOGGER.info(log)
    data = DYNAMODB.get_item(
        TableName='cfo-instance', Key={
            'InstanceName': {
                'S': instance_name
            }
        })
    return data['Item']
