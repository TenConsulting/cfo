# -*- coding: utf-8 -*-
"""
aws Security Group
"""
from logging import getLogger
from .services import EC2
from .dynamodb import u_item, g_item
LOGGER = getLogger(__name__)


def c_sec_grp(instance_name: str):
    'Create security group'
    log = '{msg:.<8} Creating security group: {}'.format(
        instance_name, msg='SEC GRP')
    LOGGER.info(log)
    security_group = EC2.create_security_group(
        Description='VPC security group {}'.format(instance_name),
        GroupName=instance_name,
        VpcId='vpc-04206e62')
    security_group_id = security_group.group_id
    u_item(instance_name, 'SecurityGroupID', 'S', security_group_id)
    log = '{msg:.<8} GroupID: {}'.format(security_group_id, msg='SEC GRP')
    LOGGER.info(log)
    security_group.create_tags(Tags=[{'Key': 'Name', 'Value': instance_name}])
    log = '{msg:.<8} Authorizing Security Group Ingress'.format(msg='SEC GRP')
    LOGGER.info(log)
    response = security_group.authorize_ingress(
        GroupId=security_group_id,
        IpPermissions=[{
            'IpProtocol': 'tcp',
            'FromPort': 80,
            'ToPort': 80,
            'IpRanges': [{
                'CidrIp': '0.0.0.0/0'
            }]
        }, {
            'IpProtocol': 'tcp',
            'FromPort': 443,
            'ToPort': 443,
            'IpRanges': [{
                'CidrIp': '0.0.0.0/0'
            }]
        }, {
            'IpProtocol': 'tcp',
            'FromPort': 22,
            'ToPort': 22,
            'IpRanges': [{
                'CidrIp': '0.0.0.0/0'
            }]
        }, {
            'IpProtocol': 'tcp',
            'FromPort': 5901,
            'ToPort': 5901,
            'IpRanges': [{
                'CidrIp': '0.0.0.0/0'
            }]
        }])
    log = '{msg:.<8} {}'.format(
        response['ResponseMetadata']['HTTPStatusCode'], msg='SEC GRP')
    LOGGER.info(log)
    return security_group_id


def d_sec_grp(instance_name: str):
    'Delete security group'
    security_group_id = g_item(instance_name)['SecurityGroupID']['S']
    log = '{msg:.<8} Deleting security group: {}'.format(
        instance_name, msg='SEC GRP')
    LOGGER.info(log)
    response = EC2.SecurityGroup(security_group_id).delete()
    log = '{msg:.<8} {}'.format(
        response['ResponseMetadata']['HTTPStatusCode'], msg='SEC GRP')
    LOGGER.info(log)
