# -*- coding: utf-8 -*-
"""
aws IAM
"""
from .services import IAM, DYNAMODB


def list_access_keys(user_name: str):
    """
    List aws user access keys
    """
    response = IAM.list_access_keys(UserName=user_name)
    for access_key in response['AccessKeyMetadata']:
        print('{} {} {}'.format(access_key['AccessKeyId'],
                                access_key['CreateDate'],
                                access_key['Status']))


def create_access_key(user_name: str):
    """
    create_access_key
    """
    response = IAM.create_access_key(UserName=user_name)
    access_key = response['AccessKey']
    response = DYNAMODB.put_item(
        TableName='cfo-access',
        Item={
            'AccessKeyId': {
                'S': access_key['AccessKeyId'],
            },
            'SecretAccessKey': {
                'S': access_key['SecretAccessKey'],
            },
            'UserName': {
                'S': access_key['UserName'],
            }
        })
    print('{} {} {}'.format(response['ResponseMetadata']['HTTPStatusCode'],
                            access_key['AccessKeyId'], access_key['UserName']))


def delete_access_key(user_name: str, access_key_id: str):
    """
    delete_access_key
    """
    IAM.delete_access_key(AccessKeyId=access_key_id, UserName=user_name)
    response = DYNAMODB.delete_item(
        TableName='cfo-access',
        Key={
            'AccessKeyId': {
                'S': access_key_id,
            },
            'UserName': {
                'S': user_name,
            }
        })
    print(response['ResponseMetadata']['HTTPStatusCode'])
