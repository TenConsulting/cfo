# -*- coding: utf-8 -*-
"""
list aws resources
"""
from .services import S3


def buckets():
    'List buckets'
    for bucket in S3.buckets.all():
        print(bucket.name)
