# -*- coding: utf-8 -*-
"""
aws services
"""
import boto3

IAM = boto3.client('iam')
DYNAMODB = boto3.client('dynamodb')
EC2 = boto3.resource('ec2')
SSM = boto3.client('ssm')
S3 = boto3.resource('s3')
