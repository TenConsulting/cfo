# -*- coding: utf-8 -*-
"""
Run bash commands fro python
"""
from subprocess import Popen, PIPE


def bash(command):
    """
    Run bash commands

    Args:
        * command: Given command

    Returns:
        * output: Output of the command
        * error: Error code of the command run

    Usage:
        output, error = bash('ls -lrt')
        print output
        print error
    """
    process = Popen(command.split(), stdout=PIPE)
    return process.communicate()
