# -*- coding: utf-8 -*-
"""
generate a password
"""
from random import choice
from string import punctuation
from haikunator import Haikunator
from xkcdpass import xkcd_password as xp


def instance_name():
    'Generate Instance Name'
    haikunator = Haikunator()
    return haikunator.haikunate(token_hex=True, token_length=6)


def password():
    'Generate Password'
    wordfile = xp.locate_wordfile()
    mywords = xp.generate_wordlist(wordfile, min_length=3, max_length=5)
    raw_password = xp.generate_xkcdpassword(mywords, numwords=4)
    words = []
    for word in raw_password.split(' '):
        words.append(word.capitalize())
    return '{}{}{}'.format(
        choice(range(9)), choice(punctuation), ''.join(words))
