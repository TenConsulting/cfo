# -*- coding: utf-8 -*-
"""
Clean pycache
"""
from os.path import join
from glob import iglob
from shutil import rmtree


def clean(pattern: str):
    """
    Remove the given pattern from the current directory

    Args:
        * pattern: Given pattern
    """
    for filename in iglob(join('**', pattern), recursive=True):
        print('Removing: ', filename)
        rmtree(filename)


clean('__pycache__')
clean('.cache')
